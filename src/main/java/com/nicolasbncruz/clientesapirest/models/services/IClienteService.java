package com.nicolasbncruz.clientesapirest.models.services;

import com.nicolasbncruz.clientesapirest.models.entity.Cliente;

import java.util.List;

public interface IClienteService {

    public List<Cliente> findAll();
}
